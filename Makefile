binaries=time-event-api

all: lint-full

test:
	@echo ">> test"

lint-full: lint card

card:
	@echo ">> card"
	@goreportcard-cli -v

lint:
	@echo ">> lint"
	@golangci-lint run

build: $(binaries)

$(binaries):
	@echo ">>build: $@ $(version)"
	@mkdir -p ./dist
	@env CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -o ./dist/$@ ./cmd/$@/main.go
	@chmod 777 ./dist/$@


swagger:
	@echo ">>swagger"
	@swag init -g /cmd/time-event-api/main.go
