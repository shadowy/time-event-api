package main

import (
	"flag"
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	swaggerFiles "github.com/swaggo/files"
	ginSwagger "github.com/swaggo/gin-swagger"
	ginLog "github.com/toorop/gin-logrus"
	_ "gitlab.com/shadowy/time-event-api/docs"
	"gitlab.com/shadowy/time-event-api/lib/router"
)

var mode = flag.String("mode", "release", "GIN mode (release, debug, test)")
var bind = flag.String("bind", "0.0.0.0:8080", "Bind address and port")

// @title Swagger Time Event API
// @version 1.0
// @description This is a Time Event Server
// @BasePath /
func main() {
	logrus.Info("time-event-api setup")
	flag.Parse()
	logrus.WithFields(logrus.Fields{"mode": *mode, "bind": *bind}).Info("time-event-api settings")
	gin.SetMode(*mode)
	if *mode == "debug" || *mode == "test" {
		logrus.SetLevel(logrus.DebugLevel)
	}

	server := gin.Default()
	server.Use(ginLog.Logger(logrus.New()), gin.Recovery())
	err := router.InitRoutes(server)
	if err != nil {
		logrus.WithError(err).Error("docker-deploy init routes")
		return
	}
	server.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler))

	logrus.Info("docker-deploy start")
	err = server.Run(*bind)
	if err != nil {
		logrus.WithError(err).Error("docker-deploy")
	}
}
