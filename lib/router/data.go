package router

import (
	"github.com/gin-gonic/gin"
	uuid "github.com/satori/go.uuid"
	"github.com/sirupsen/logrus"
	"gitlab.com/shadowy/time-event-api/lib/data"
	"gitlab.com/shadowy/time-event-api/lib/router/model"
	"net/http"
)

type dataController struct {
	entity *data.Entity
}

func initData(root *gin.RouterGroup) error {
	router := root.Group("data")
	controller := dataController{entity: &data.Entity{BasePath: "./data"}}
	router.GET("/:system/:entity", controller.list)
	router.GET("/:system/:entity/:id", controller.item)
	router.POST("/:system/:entity", controller.add)
	router.PUT("/:system/:entity/:id", controller.update)
	router.DELETE("/:system/:entity/:id", controller.delete)
	return nil
}

// @Description Get list of entities
// @Tags Service
// @Accept  json
// @Produce  json
// @Param system path string true "System name"
// @Param entity path string true "Entity name"
// @Success 200 {object} []model.DataItem	"List of entities"
// @Failure 500 {object} model.ResponseError "Internal error"
// @Router /api/data/{system}/{entity} [get]
func (c dataController) list(ctx *gin.Context) {
	system := ctx.Param("system")
	entity := ctx.Param("entity")
	logrus.WithFields(logrus.Fields{"system": system, "entity": entity}).Debug("dataController.list")
	list, err := c.entity.LoadData(system, entity)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, model.ResponseError{Error: err.Error()})
		return
	}
	ctx.JSON(http.StatusOK, list)
}

// @Description Get entity by id
// @Tags Service
// @Accept  json
// @Produce  json
// @Param system path string true "System name"
// @Param entity path string true "Entity name"
// @Param id path string true "Entity id"
// @Success 200 {object} model.DataItem	"Entity"
// @Failure 404 {object} model.ResponseError "Entity not found"
// @Failure 500 {object} model.ResponseError "Internal error"
// @Router /api/data/{system}/{entity}/{id} [get]
func (c dataController) item(ctx *gin.Context) {
	system := ctx.Param("system")
	entity := ctx.Param("entity")
	id := ctx.Param("id")
	logrus.WithFields(logrus.Fields{"system": system, "entity": entity, "id": id}).Debug("dataController.item")
	list, err := c.entity.LoadData(system, entity)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, model.ResponseError{Error: err.Error()})
		return
	}
	for i := range list {
		el := list[i]
		if *el.ID == id {
			ctx.JSON(http.StatusOK, el)
			return
		}
	}
	ctx.JSON(http.StatusNotFound, model.ResponseError{Error: "Entity not found"})
}

// @Description Add entity
// @Tags Service
// @Accept  json
// @Produce  json
// @Param system path string true "System name"
// @Param entity path string true "Entity name"
// @Param entity body model.DataItem true "Entity"
// @Success 200 {object} model.DataItem	"Entity"
// @Failure 406 {object} model.ResponseError "Data not valid"
// @Failure 500 {object} model.ResponseError "Internal error"
// @Router /api/data/{system}/{entity} [post]
func (c dataController) add(ctx *gin.Context) {
	system := ctx.Param("system")
	entity := ctx.Param("entity")
	logrus.WithFields(logrus.Fields{"system": system, "entity": entity}).Debug("dataController.add")
	list, err := c.entity.LoadData(system, entity)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, model.ResponseError{Error: err.Error()})
		return
	}
	var m model.DataItem
	if err = ctx.BindJSON(&m); err != nil {
		ctx.JSON(http.StatusNotAcceptable, model.ResponseError{Error: err.Error()})
		return
	}
	id := uuid.NewV4().String()
	m.ID = &id
	list = append(list, m)
	err = c.entity.SaveData(system, entity, list)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, model.ResponseError{Error: err.Error()})
		return
	}
	ctx.JSON(http.StatusOK, m)
}

// @Description Update entity
// @Tags Service
// @Accept  json
// @Produce  json
// @Param system path string true "System name"
// @Param entity path string true "Entity name"
// @Param id path string true "Entity id"
// @Param entity body model.DataItem true "Entity"
// @Success 200 {object} model.DataItem	"Entity"
// @Failure 404 {object} model.ResponseError "Entity not found"
// @Failure 406 {object} model.ResponseError "Data not valid"
// @Failure 500 {object} model.ResponseError "Internal error"
// @Router /api/data/{system}/{entity}/{id} [put]
func (c dataController) update(ctx *gin.Context) {
	system := ctx.Param("system")
	entity := ctx.Param("entity")
	id := ctx.Param("id")
	logrus.WithFields(logrus.Fields{"system": system, "entity": entity, "id": id}).Debug("dataController.update")
	var m model.DataItem
	if err := ctx.BindJSON(&m); err != nil {
		ctx.JSON(http.StatusNotAcceptable, model.ResponseError{Error: err.Error()})
		return
	}
	list, err := c.entity.LoadData(system, entity)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, model.ResponseError{Error: err.Error()})
		return
	}
	for i := range list {
		el := list[i]
		if *el.ID == id {
			el.Data = m.Data
			list[i] = el
			err = c.entity.SaveData(system, entity, list)
			if err != nil {
				ctx.JSON(http.StatusInternalServerError, model.ResponseError{Error: err.Error()})
				return
			}
			ctx.JSON(http.StatusOK, el)
			return
		}
	}
	ctx.JSON(http.StatusNotFound, model.ResponseError{Error: "Entity not found"})
}

// @Description Delete entity
// @Tags Service
// @Accept  json
// @Produce  json
// @Param system path string true "System name"
// @Param entity path string true "Entity name"
// @Param id path string true "Entity id"
// @Success 204
// @Failure 500 {object} model.ResponseError "Internal error"
// @Router /api/data/{system}/{entity}/{id} [delete]
func (c dataController) delete(ctx *gin.Context) {
	system := ctx.Param("system")
	entity := ctx.Param("entity")
	id := ctx.Param("id")
	logrus.WithFields(logrus.Fields{"system": system, "entity": entity, "id": id}).Debug("dataController.delete")
	list, err := c.entity.LoadData(system, entity)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, model.ResponseError{Error: err.Error()})
		return
	}
	var newList []model.DataItem
	for i := range list {
		el := list[i]
		if *el.ID != id {
			newList = append(newList, el)
		}
	}
	err = c.entity.SaveData(system, entity, newList)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, model.ResponseError{Error: err.Error()})
		return
	}
	ctx.Status(http.StatusNoContent)
}
