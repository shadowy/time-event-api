package model

type DataItem struct {
	ID   *string `json:"id"`
	Data string  `json:"data"`
}
