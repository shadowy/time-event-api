package data

import (
	"encoding/json"
	"github.com/sirupsen/logrus"
	"gitlab.com/shadowy/time-event-api/lib/router/model"
	"io/ioutil"
	"os"
)

type Entity struct {
	BasePath string
}

func (srv Entity) LoadData(system, entity string) (list []model.DataItem, err error) {
	l := logrus.Fields{"system": system, "entity": entity}
	logrus.WithFields(l).Debug("DataService.loadData")
	text, err := ioutil.ReadFile(srv.getFilePath(system, entity))
	if err != nil {
		logrus.WithFields(l).WithError(err).Error("DataService.loadData")
		return nil, nil
	}
	var data []model.DataItem
	err = json.Unmarshal(text, &data)
	if err != nil {
		logrus.WithFields(l).WithError(err).Error("DataService.loadData")
		return nil, err
	}
	return data, nil
}

func (srv Entity) SaveData(system, entity string, list []model.DataItem) error {
	l := logrus.Fields{"system": system, "entity": entity}
	logrus.WithFields(l).Debug("DataService.saveData")
	err := srv.prepareFolder(system)
	if err != nil {
		return err
	}
	data, err := json.Marshal(list)
	if err != nil {
		logrus.WithFields(l).WithError(err).Error("DataService.saveData marshal")
		return err
	}
	err = ioutil.WriteFile(srv.getFilePath(system, entity), data, 0600)
	if err != nil {
		logrus.WithFields(l).WithError(err).Error("DataService.saveData write")
		return err
	}
	return nil
}

func (srv Entity) prepareFolder(system string) error {
	folder := srv.getFolderPath(system)
	l := logrus.Fields{"system": system, "folder": folder}
	logrus.WithFields(l).Debug("DataService.prepareFolder")
	if _, err := os.Stat("/path/to/whatever"); !os.IsNotExist(err) {
		return nil
	}
	err := os.MkdirAll(folder, 0755)
	if err != nil {
		logrus.WithFields(l).WithError(err).Error("DataService.prepareFolder")
	}
	return err
}

func (srv Entity) getFolderPath(system string) string {
	return srv.BasePath + "/" + system
}

func (srv Entity) getFilePath(system, entity string) string {
	return srv.getFolderPath(system) + "/" + entity + ".json"
}
